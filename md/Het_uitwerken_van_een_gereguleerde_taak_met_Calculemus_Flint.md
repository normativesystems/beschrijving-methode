

## 1. Het definiëren van een taak

### Wat is een taak
Een taak wordt gedefinieerd door een doel. Dat wat wordt bereikt door het uitvoeren van een taak, dat wat waar wordt door het uitvoeren van een taak.

Een taak kan eenvoudig (simpel), of samengesteld (complex) zijn. Een samengestelde taak bestaat uit:
- een bovenliggende taak,
- een taak, en/of
- een subtaak.

### Werkwijze
Wanneer een taak wordt gedefinieerd door een groep, kan de onderstaande werkwijze helpen om tot een gezamenlijke taakdefinitie te komen.

#### Verzamelen taken
Alle deelnemers maken een taakbeschrijving, of een beschrijving van een deel van de taak. De beschrijvingen worden verzameld.

#### Ordenen  taken
De beschrijvingen worden geordend naar bovenliggende taken, taken en subtaken. De ordening wordt besproken. Doel is het totstandkomen van een gezamenlijke ordening.

Soms is het nuttig om de taakdefinitie op te splitsen in een primaire taak (het doel dat wordt nagestreefd door de groep) en een secundaire taak (de wijze waarop aan de primaire taak wordt gewerkt).

#### Vaststellen taak  
Op basis van de gezamenlijke ordening wordt een taakbeschrijving gemaakt. Het vervolg van het protocol worden op basis van deze beschrijving doorlopen. Als daar behoefte is kan de taakbeschrijving op een later moment worden aangepast of aangescherpt.

#### Bijstellen taak
Een taak kan naar behoefte worden bijgesteld. Redenen voor het bijstellen van taak kan zijn dat:
- deelnemers van de groep hun opvattingen bijstellen,
- het verzamelen en interpreteren van bronnen leidt tot nieuwe ideeën over de taak,
- de taak wordt bijgesteld tijdens het oplossen van meningsverschillen.

## 2. Het verzamelen van bronnen

### Wat zijn bronnen?
Alles dat ten grondslag ligt aan het reguleren van gedrag is een bron. Wanneer het gaat om bronnen die het reguleren van overheidshandelen, dan maken in Nederland, algemeen verbindende voorschriften (wetten, algemene maatregelen van bestuur, ministeriële regelingen of regelingen van een zelfstandig bestuursorgaan) deel uit van de bronnen waarin kennis over het reguleren van gedrag is vastgelegd. In de EU gaat het dan om al dan niet bindende rechtshandelingen (waaronder verordeningen, richtlijnen, besluiten, aanbevelingen).

Andere bronnen zijn:
- interne regels,
- beleidsregels,
- jurisprudentie,
- niet juridische bronnen waaronder teksten die hoofdzakelijk op de werkelijkheid betrekking hebben (non-fictie), levensbeschouwelijke werken en verhalen (fictie).

### Welke bronnen zijn relevant voor een taak?

### Werkwijze
Op basis van de taakomschrijving en de domeinkennis van de deelnemers wordt een verzameling bronnen gemaakt.

De regelgevende werking van deze bronnen wordt bepaald door de interpretatie ervan. Als een belanghebbende van mening is dat een bron relevant is voor het uitvoeren van een taak, is de enige manier om er achter te komen of dat zo is: het interpreteren van die bron. Pas na interpretatie kan worden vastgesteld dat de bron bij nader inzien toch niet relevant is.

Wees niet bang dat je te weinig bronnen hebt, je kunt later altijd bronnen toevoegen. Het nadeel van teveel bronnen is dat het veel werk is om ze allemaal te interpreteren. Begin bij voorkeur met een kleine verzameling, en breidt deze zonodig uit

#### Decompositie van bronnen
Om te voorkomen dat hele wetten geïnterpreteerd moeten worden, als ook maak één zin relevant is voor de zaak, worden bronnen in losse componenten uit elkaar gehaald.

Op basis van het uitgangspunt is dat de 'zin' betekenis draagt, worden bronnen in losse zinnen uit elkaar gehaald. De plaats van de zin in de bron wordt vervolgens vastgelegd. Een zin kan op meerdere plekken in dezelfde bron voorkomen, maar ook in meerdere bronnen. De zin wordt eenmalig opgeslagen. In de metadata wordt vastgelegd waar de zin is terug te vinden, vanaf wanneer en, indien relevant, tot wanneer de zin op die plek stond.

Ook de structuurkenmerken (zoals hoofdstuk, paragraaf, artikel, lid) van de bron, en de titels daarvan worden vastgelegd. Dit omdat in deze titels soms kennis is vastgelegd die niet op andere plekken is terug te vinden.

Als een zin een opsomming bevat wordt de zin opgeknipt in de aanhef, de onderdelen van de opsomming en, soms, het slot. Dit omdat de onderdelen van een opsomming vaak op zichzelf staande kennis bevatten.

Voor de decompositie van bronnen hebben we een instrument ontwikkeld. Voor het basiswettenbestand, dat omvangrijk is en teksten bevat die regelmatig worden aangepast, is het wenselijk om een specifieke oplossing te maken. Suggesties hiervoor heeft TNO in samenwerking met Coherenza uitgewerkt.

## 3. Het interpreteren van bronnen
Het interpreteren van teksten is de kern van het maken van regels. Het zoeken van fundamentele concepten voor het beschrijven van normen is bedoeld om:
- het interpreteren van normen zo helder mogelijk te beschrijven,
- het automatisch ondersteunen van interpretatiewerkzaamheden mogelijk te maken,
- verschillende interpretatiemethoden te kunnen vergelijken (want als een andere methode niet is om te zetten in fundamentele normatieve concepten, dan voldoen deze concepten dus (nog) niet en is aanpassing van de taal nodig).

### Wat is interpreteren?
De algemene betekenis van interpreteren is verklaren of uitleggen.  In de context van norm engineering is interpreteren het systematisch betekenis geven aan zinnen en fragmenten. Het interpreteren van een norm is het expliciet beschrijven welk gedrag vanuit sociaal, ethisch of juridisch perspectief gewenst is.

Door interpretatie worden normen uit een bron gehaald (geëxtraheerd).

### Hoe kies je een interpretatiemethode?
Interpretatie kan op verschillende manieren. Hoe kies je een interpretatiemethode die past bij de context waarin je interpretaties wilt gebruiken?

Er zijn vier manieren om bronnen van normen te interpreteren:
- de zaakgerichte (case-based) benadering als je normen wilt gebruiken in de context van een rechtszaak;
- de regelgebaseerde (rule-based) benadering (als je op zoekt bent naar regels van het type 'als X dan Y';
- deontische logica (als je op zoek bent naar verplichtingen, permissies, verboden en overtredingen);
- de modelmatige (model-based) benadering als je op zoek bent naar een generieke methode, of een methode die je op maat kunt maken voor het specifieke vraagstuk waaraan je werkt.

Voorbeelden van de modelmatige benadering zijn Calculemus-Flint en Wetsanalyse.

### Werkwijze Flint interpretatie
Hieronder is beschreven hoe Flint interpretaties worden gemaakt. Voor wie graag werkt aan de hand van voorbeelden, voegen we voorbeeldinterpretaties toe (pm).

#### Op zoek naar acties, actoren, objecten en ontvangers
Het interpreteren van bronnen begint met het zoeken naar acties, actors, objecten en ontvangers.

We zijn niet op zoek naar alle acties, maar naar normatieve acties; acties die sociaal, ethisch, of juridisch gewenst zijn. We zijn op zoek naar acties die een resultaat hebben. De actie is gewenst omdat hij een gevolg heeft. Een actie zonder gevolg is geen normatieve actie omdat hij niets verandert in de wereld.

Dit is een soort zinsontleden. In een zin in de bedrijvende vorm is de actie het gezegde, de actor het onderwerp, het object het lijdend voorwerp (direct object) en de ontvanger het ontvangend voorwerp (indirect object).

Lang niet altijd staan acties, actoren, objecten en ontvangers in één zin. Vaak zijn ze verdeeld over meerdere zinnen.

Soms zijn niet alle onderdelen van een handeling terug te vinden in een bron, dan zijn er drie mogelijkheden:
- dit is geen normatieve handeling,
- er zijn aanvullende bronnen nodig om een volledige interpretatie te maken, of
- een bron bevat impliciete informatie.

Een voorbeeld van impliciete informatie is artikel 1:3 lid 3 van de Algemene wet bestuursrecht: "Onder aanvraag wordt verstaan: een verzoek van een belanghebbende, een besluit te nemen."

In deze zin staat dat er iets is dat we 'aanvraag' noemen, en dat dat het "verzoek van een belanghebbende, een besluit te nemen" is. Maar in deze zin, noch enige andere zin van de Algemene wet bestuursrecht, staat expliciet dat een 'belanghebbende' (actor) een 'verzoek' (object) kunnen 'indienen' (actie) om een besluit te nemen. 

Als we zo'n handeling willen maken, dan legt een interpretator expliciet vast dat hij verondersteld dat uit de definitie van het concept 'aanvraag' impliciet volgt dat belanghebbenden een verzoek kunnen indienen dat telt als een aanvraag.

#### Voorwaarden en resultaten
Over het algemeen zijn de  acties, actoren, objecten en ontvangers van een handeling het makkelijkst te vinden. De voorwaarden waaraan een handeling moet voldoen, en het resultaat ervan, vragen vaak wat meer moeite.

Bij het uitwerken van de voorwaarden zijn er twee complicaties:
- Hoe weet je of je alle voorwaarden hebt gevonden?
- Moet een voorwaarde nog verder in detail worden uitgewerkt?

Of je alle voorwaarden hebt gevonden, weet je nooit. Door vast te leggen welke voorwaarden gelden, stel je anderen in de gelegenheid om met aanvullende voorwaarden te komen, of te beargumenteren waarom een voorwaarde niet geldt.

Detaillering van voorwaarden is nodig als verwacht wordt dat één van de betrokkenen behoefte heeft aan gedetailleerde voorwaarden, of als de betrokkenen het niet eens kunnen worden of wel of niet aan de voorwaarden is/wordt voldaan.

#### Van actie naar handeling
Normatieve acties zijn acties die een actor hebben. En die een ontvanger voor wie het resultaat bestemd is. Meestal hebben acties ook een object, het ding waar ze betrekking op hebben (zoals een aanvraag het object is van het inwilligen of afwijzen van die aanvraag).

Het resultaat van de actie ontstaat alleen als deze geldig is, als de actie is uitgevoerd  op een wijze die voldoet aan de voorwaarden.

#### Uitwerken handelingen
Een uitgewerkte handeling is een handeling die in ieder geval een actie, een actor, een ontvanger en een resultaat heeft. De meeste handelingen hebben ook een object en voorwaarden, maar dit is niet noodzakelijk. Dit zijn de componenten van een handeling.

|identiteit     | ...                                   |
| :------------ | :------------                         |
| *handeling*     | 'actor'-'actie'-'object'-'ontvanger'|
| *actor*         | ...                                 |
| *actie*         | ...                                 |
| *object*        | ...                                 |
| *ontvanger*     | ...                                 |
| *voorwaarden*   | ...                                 |
| *resultaat*     | ...                                 |
| *bron*          | ...                                 |

De bovenstaande tabel bevat alle componenten van een handeling. Daarnaast bevat de tabel de minimale set metadata van een handeling:  de identiteit en de bron.

Een handeling heeft een unieke identiteit. Daarnaast heeft de handeling een leesbare naam. De default naam van een handeling is 'actor'-'actie'-'object'-'ontvanger'. Desgewenst kan een beter leesbare, of compacte alternatieve naam worden toegevoegd.

De bron van een handeling bestaat uit de referenties naar de bronnen waar de actie, de actor, het object, de ontvanger en het resultaat staan vermeld. Omdat de componenten van een handeling vaak meerdere keren in een bron voorkomen, heeft de handeling een eigen bron: het tekstfragment, of de tekstfragmenten waar de handeling betrekking op heeft. Dus niet 'aanvraag' als concept dat op allerlei plaatsen in Nederlandse wet- en regelgeving voorkomt, maar 'aanvraag' zoals bedoeld in artikel 1:3 Algemene wet bestuursrecht, is het resultaat van het indienen van een verzoek tot het nemen van een besluit door een bestuursorgaan. 

De voorwaarde is meestal een complex feit die bestaat uit meerdere onderdelen en de relaties tussen die onderdelen. Deze worden apart uitgewerkt.

Voorbeelden van het interpreteren van handelingen worden later toegevoegd.

#### Uitwerken voorwaarden
De componenten van een handelingen zijn feiten.

|identiteit     | ...                                   |
| :------------ | :------------                         |
| *feit*          | ...                                 |
| *functie*       | 'de functie die een feit waar maakt'|
| *bron*          | ...                                 |



## 4. Het uitvoeren van de taak

### Instructies voor het uitvoeren van een taak

Wat zijn de regels (criteria)
De zaak (bewijsmiddelen)

### Specificaties voor automatisch redeneren

Wat zijn de regels (criteria)
De zaak (bewijsmiddelen)

## 5. Het vergelijken van uitkomsten

Verschillende uitkomsten

Oorzaak verschillen
- meningsverschil over taak
- meningsverschil over relevante bronnen
- meningsverschil over een interpretatie
- meningsverschil over de bewijsmiddelen in de zaak

Oplossen meningsverschillen
- bijstellen taak
- aanvullen bronnen
- aanpassen interpretaties
- bewijsmiddelen in de zaak

Oplossen meningsverschillen

