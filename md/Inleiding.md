*Vorige: [Inhoudsopgave](https://gitlab.com/normativesystems/beschrijving-methode/-/wikis/Home)*<br />
*Volgende: [Normatieve systemen voor het beïnvloeden van gedrag](https://gitlab.com/normativesystems/beschrijving-methode/-/wikis/Normatieve-systemen-voor-het-be%C3%AFnvloeden-van-gedrag)*

---

Normen beïnvloeden ons gedrag. Dat is een nadeel omdat we niet zomaar alles kunnen doen dat in ons hoofd opkomt, maar het levert ook veel op. Hoe dat werkt is het onderwerp van de studie van normatieve systemen.

We zijn op zoek naar de bouwstenen van normen, hoe ze ons gedrag beïnvloeden, hoe we ze systematisch kunnen beschrijven, we willen weten hoe we normen toepassen in specifieke zaken.

Deze wiki behandelt als deze onderwerpen. Hij bevat beknopte informatie voor wie op zoek is naar praktisch bruikbare methoden, en theoretische onderbouwing voor wie die zoekt (of een bijdrage wil leveren aan de ontwikkeling van theoretische kennis over normatieve systemen).

In dit project wordt ook gewerkt aan instrumenten voor het maken en gebruiken van normatieve systemen. We presenteren de instrumenten die we ontwikkelen, inclusief links waar je ze kunt uitproberen. We geven instructies om ze te gebruiken, we nodigen je uit om mee te werken aan deze instrumenten, of nieuwe instrumenten te initiëren.

Dit is werk in uitvoering. Als je vragen of opmerkingen hebt, horen we het graag. Als je wilt meedoen nog liever! We werken aan een internationale standaard voor het werken met normen.

Wil je meer weten: via de onderstaande pagina's kun je vinden wat je zoekt.

- [Normatieve systemen voor het beïnvloeden van gedrag](https://gitlab.com/normativesystems/beschrijving-methode/-/wikis/Normatieve-systemen-voor-het-be%C3%AFnvloeden-van-gedrag)
- [De overheid als normatief systeem](https://gitlab.com/normativesystems/beschrijving-methode/-/wikis/De-overheid-als-normatief-systeem)
- [Perspectieven op normatieve systemen](https://gitlab.com/normativesystems/beschrijving-methode/-/wikis/Perspectieven-op-normatieve-systemen)
- [De Calculemus-Flint methode](https://gitlab.com/normativesystems/beschrijving-methode/-/wikis/De-Calculemus-Flint-methode)
- [Voorbeelden van het werken met Calculemus-Flint](https://gitlab.com/normativesystems/beschrijving-methode/-/wikis/Voorbeelden-van-het-werken-met-Calculemus-Flint)
- [Instrumenten voor het maken en gebruiken van normatieve systemen](https://gitlab.com/normativesystems/beschrijving-methode/-/wikis/Instrumenten-voor-het-maken-en-gebruiken-van-normatieve-systemen)
- [Hoe verder?](https://gitlab.com/normativesystems/beschrijving-methode/-/wikis/Hoe-verder%3F)