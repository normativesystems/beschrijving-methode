*Vorige: [Instrumenten voor het maken en gebruiken van normatieve systemen](https://gitlab.com/normativesystems/beschrijving-methode/-/wikis/Instrumenten-voor-het-maken-en-gebruiken-van-normatieve-systemen)*<br />
*-Einde-*

We werken verder aan het ontwikkelen van praktisch toepasbare theorie en bijbehorende instrumenten. Dat doen we aan de hand van casuïstiek. De belangstelling van gebruikers, en de vragen die we in de praktijk tegenkomen bepalen het tempo en de richting van de ontwikkeling.

---

*-Einde-*