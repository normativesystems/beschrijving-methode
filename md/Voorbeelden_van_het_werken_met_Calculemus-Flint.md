*Volgende: [De Calculemus Flint methode](https://gitlab.com/normativesystems/beschrijving-methode/-/wikis/De-Calculemus-Flint-methode)*<br />
*Volgende: [Instrumenten voor het maken en gebruiken van normatieve systemen](https://gitlab.com/normativesystems/beschrijving-methode/-/wikis/Instrumenten-voor-het-maken-en-gebruiken-van-normatieve-systemen)*

---

Op deze plek zijn links te vinden naar toepassingen van de Calculemus-Flint methode:

- [voorbeeld](https://gitlab.com/normativesystems/beschrijving-methode/-/wikis/Voorbeeld-interpreteren:-inwilligen-aanvraag-verblijfsvergunning-voor-bepaalde-tijd) interpretatie vreemdelingenwet voor het inwilligen van een aanvraag tot het verlenen van een reguliere verblijfsvergunning voor bepaalde tijd
- een [presentatie](https://docs.google.com/presentation/d/1Xpa2S-4hYdiTZHSEeQ5OT7ay-8zFPQBu798jfE4EbIc/edit?usp=sharing) over norm engineering en de toepassing van Calculemus-Flint in een presentatie bestaande uit een algemene inleiding op de methode, en de toepassing van de methode op een subsidieregeling van de provincie Fryslan.

---

*Volgende: [Instrumenten voor het maken en gebruiken van normatieve systemen](https://gitlab.com/normativesystems/beschrijving-methode/-/wikis/Instrumenten-voor-het-maken-en-gebruiken-van-normatieve-systemen)*